# Master (will become release 2.8)

- constructBoundaryDofs:
    - Small interface change in the template parameters: drop `blocksize` and replace it by `BitSetVector`
    - The method can now handle generic `dune-functions` basis types, as long as we have consistency in the data types

## Deprecations and removals

- ...
