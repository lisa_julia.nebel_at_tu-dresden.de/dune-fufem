project("dune-fufem" CXX)
cmake_minimum_required(VERSION 2.8.6)

if(NOT (dune-common_DIR OR dune-common_ROOT))
  string(REPLACE ${CMAKE_PROJECT_NAME} dune-common dune-common_DIR ${PROJECT_BINARY_DIR})
endif()

find_package(dune-common REQUIRED)
list(APPEND CMAKE_MODULE_PATH ${dune-common_MODULE_PATH})
include(DuneMacros)

dune_project()

add_subdirectory("doc")
add_subdirectory("dune")

finalize_dune_project(GENERATE_CONFIG_H_CMAKE)
