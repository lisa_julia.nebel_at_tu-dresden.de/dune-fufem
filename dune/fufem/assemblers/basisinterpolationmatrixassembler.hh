// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_FUFEM_ASSEMBLERS_BASIS_INTERPOLATION_MATRIX_ASSEMBLER_HH
#define DUNE_FUFEM_ASSEMBLERS_BASIS_INTERPOLATION_MATRIX_ASSEMBLER_HH

#include <type_traits>
#include <typeinfo>

#include <dune/common/bitsetvector.hh>

#include <dune/istl/matrixindexset.hh>
#include <dune/istl/scaledidmatrix.hh>

#include <dune/matrix-vector/addtodiagonal.hh>

#include <dune/fufem/functions/cachedcomponentwrapper.hh>
#include <dune/localfunctions/common/virtualinterface.hh>

/** \brief Wrapper that extracts a single local basis function. */
template<class LocalBasis, class Base>
class LocalBasisComponentWrapper :
    public CachedComponentWrapper<
        LocalBasisComponentWrapper<LocalBasis, Base>,
        std::vector<typename LocalBasis::Traits::RangeType>,
        Base>
{
  public:
    typedef std::vector<typename LocalBasis::Traits::RangeType> AllRangeType;

  private:
    typedef CachedComponentWrapper<LocalBasisComponentWrapper<LocalBasis, Base>, AllRangeType, Base> BaseClass;

  public:
    typedef typename Base::DomainType DomainType;
    typedef typename Base::RangeType RangeType;

        LocalBasisComponentWrapper(const LocalBasis& localBasis, int comp) :
            BaseClass(comp),
            localBasis_(localBasis)
        {}

        void evaluateAll(const DomainType& x, AllRangeType& y) const
        {
            localBasis_.evaluateFunction(x, y);
        }

    protected:
        const LocalBasis& localBasis_;
};

/** \brief Method that computes the interpolation matrix mapping coefficients from one basis to
 *         the coefficients of another basis.
 *         Note that the interpolation only works exactly if one basis can be represented by the other.
 *         (Here denoted by coarse and fine basis)
 *         In this case multiplication of the matrix from the left represents the interpolation.
 */
template<class MatrixType, class BasisType0, class BasisType1>
static void assembleBasisInterpolationMatrix(MatrixType& matrix, const BasisType0& coarseBasis, const BasisType1& fineBasis)
{
    typedef typename BasisType0::GridView GridView;

    static_assert(std::is_same<GridView, typename BasisType1::GridView>::value, "GridView's don't match!");

    const GridView& gridView = coarseBasis.getGridView();

    int rows = fineBasis.size();
    int cols = coarseBasis.size();

    typedef typename BasisType0::LocalFiniteElement FEType0;
    typedef typename BasisType1::LocalFiniteElement FEType1;
    typedef typename Dune::LocalFiniteElementFunctionBase<FEType0>::type FunctionBaseClass;
    typedef LocalBasisComponentWrapper<typename FEType0::Traits::LocalBasisType, FunctionBaseClass> LocalBasisWrapper;

    matrix.setSize(rows,cols);
    matrix.setBuildMode(MatrixType::random);

    auto eIt    = gridView.template begin<0>();
    auto eEndIt = gridView.template end<0>();

    // ///////////////////////////////////////////
    // Determine the indices present in the matrix
    // /////////////////////////////////////////////////

    // Only handle every dof once
    Dune::BitSetVector<1> processed(fineBasis.size());
    for (size_t i=0; i<processed.size();i++)
        if (fineBasis.isConstrained(i))
            processed[i] = true;

    Dune::MatrixIndexSet indices(rows, cols);

    for (; eIt != eEndIt; ++eIt) {

        // Get local finite elements
        const FEType0& lfe0 = coarseBasis.getLocalFiniteElement(*eIt);
        const FEType1& lfe1 = fineBasis.getLocalFiniteElement(*eIt);

        const size_t numBaseFct0 = lfe0.localBasis().size();
        const size_t numBaseFct1 = lfe1.localBasis().size();

        // check if all components have already been processed
        bool allProcessed = true;
        for(size_t i=0; i<numBaseFct1; ++i)
            allProcessed = allProcessed and processed[fineBasis.index(*eIt, i)][0];

        // preallocate vector for function evaluations
        std::vector<typename FEType0::Traits::LocalBasisType::Traits::RangeType> values(numBaseFct0);

        // Extract single basis functions into a format that can be used within local interpolation
        LocalBasisWrapper basisFctj(lfe0.localBasis(),0);

        for (size_t j=0; j<numBaseFct0; j++)
        {
            // wrap each local basis function as a local function.
            basisFctj.setIndex(j);

            // Interpolate j^th base function by the fine basis
            lfe1.localInterpolation().interpolate(basisFctj, values);

            int globalCoarse = coarseBasis.index(*eIt,j);

            for (size_t i=0; i<numBaseFct1; i++) {

                if (std::abs(values[i])<1e-12)
                    continue;

                int globalFine = fineBasis.index(*eIt,i);

                if (processed[globalFine][0])
                    continue;

                if (coarseBasis.isConstrained(globalCoarse)) {

                    const auto& lin = coarseBasis.constraints(globalCoarse);

                    for (size_t k=0; k<lin.size(); k++)
                        indices.add(globalFine, lin[k].index);
                } else
                    indices.add(globalFine, globalCoarse);
            }

        }

        for (size_t i=0; i<numBaseFct1; i++)
            processed[fineBasis.index(*eIt,i)]=true;
    }

    indices.exportIdx(matrix);
    matrix = 0;

    // /////////////////////////////////////////////
    // Compute the matrix
    // /////////////////////////////////////////////

    // Only handle every dof once
    processed.unsetAll();
    for (size_t i=0; i<processed.size();i++)
        if (fineBasis.isConstrained(i))
            processed[i] = true;

    eIt    = gridView.template begin<0>();
    for (; eIt != eEndIt; ++eIt) {

        // Get local finite element
        const FEType0& lfe0 = coarseBasis.getLocalFiniteElement(*eIt);
        const FEType1& lfe1 = fineBasis.getLocalFiniteElement(*eIt);

        const size_t numBaseFct0 = lfe0.localBasis().size();
        const size_t numBaseFct1 = lfe1.localBasis().size();

        // check if all components have already been processed
        bool allProcessed = true;
        for(size_t i=0; i<numBaseFct1; ++i)
            allProcessed = allProcessed and processed[fineBasis.index(*eIt, i)][0];

        if (not allProcessed) {

            // preallocate vector for function evaluations
            std::vector<typename FEType0::Traits::LocalBasisType::Traits::RangeType> values(numBaseFct0);

            // Extract single basis functions into a format that can be used within local interpolation
            LocalBasisWrapper basisFctj(lfe0.localBasis(),0);

            for (size_t j=0; j<numBaseFct0; j++)
            {
                // wrap each local basis function as a local function.
                basisFctj.setIndex(j);

                int globalCoarse = coarseBasis.index(*eIt,j);

                // Interpolate j^th base function by the fine basis
                lfe1.localInterpolation().interpolate(basisFctj, values);

                for (size_t i=0; i<numBaseFct1; i++) {

                    if (std::abs(values[i])<1e-12)
                        continue;

                    int globalFine = fineBasis.index(*eIt,i);
                    if (processed[globalFine][0])
                        continue;

                    if (coarseBasis.isConstrained(globalCoarse)) {

                        const auto& lin = coarseBasis.constraints(globalCoarse);

                        for (size_t k=0; k<lin.size(); k++)
                            Dune::MatrixVector::addToDiagonal(matrix[globalFine][lin[k].index],lin[k].factor*values[i]);
                    } else
                        Dune::MatrixVector::addToDiagonal(matrix[globalFine][globalCoarse],values[i]);
                }
            }

            for (size_t i=0; i<numBaseFct1; i++)
                    processed[fineBasis.index(*eIt,i)] = true;
        }
    }
}






/** \brief Wrapper for evaluating local functions in another geometry */
template<class Function, class Geometry>
class OtherGeometryWrapper
{

public:

  using Traits = typename Function::Traits;

  OtherGeometryWrapper(const Function& function, const Geometry& geometry)
  : function_(function)
  , geometry_(geometry)
  {}

  // forward the evaluation into the other geometry
  template<class X, class Y>
  void evaluate(const X& x, Y& y) const
  {
    function_.evaluate(geometry_.global(x), y);
  }

private:

  const Function function_;
  const Geometry geometry_;
};


/** \brief Wrapper chaining sequencial father geometries */
template<class GeometryContainer>
class GeometryInFathers
{

public:

  GeometryInFathers(const GeometryContainer& geometryContainer)
  : geometryContainer_(geometryContainer)
  {}

  // forward the global() method to all single geometries
  template<class X>
  auto global(const X& x) const
  {
    auto y = x;
    for (const auto& g : geometryContainer_)
    {
      y = g.global(y);
    }
    return y;
  }

private:

  const GeometryContainer geometryContainer_;
};



/** \brief Assemble the transfer matrix for multigrid methods
 *
 *  The coarse and fine basis has to be related: in a way that the fine grid is descendant of the
 *  coarse grid (not necessarily direct descandant).
 *  The matrix is assumed to be in block structure with correct block size.
 *  The two basis are assumed to be scalar dune-functions basis (i.e. leaf nodes in the basis tree).
 *
 * \param [out] matrix the block matrix corresponding to the basis transfer operator
 * \param [in] coarseBasis scalar global basis on the coarse grid
 * \param [in] fineasis scalar global basis on the fine grid
 * \param [in] tolerance (optional) interpolation threshold. Default is 1e-12.
 */
template<class MatrixType, class CoarseBasis, class FineBasis>
static void assembleGlobalBasisTransferMatrix(MatrixType& matrix,
                                              const CoarseBasis& coarseBasis,
                                              const FineBasis& fineBasis,
                                              const double tolerance = 1e-12)
{
  const auto& coarseGridView = coarseBasis.gridView();
  const auto& fineGridView   = fineBasis.gridView();

  auto coarseLocalView = coarseBasis.localView();
  auto fineLocalView   = fineBasis.localView();

  const auto rows = fineBasis.size();
  const auto cols = coarseBasis.size();

  if ( not fineLocalView.tree().isLeaf or not coarseLocalView.tree().isLeaf )
    DUNE_THROW(Dune::Exception, "currently, we can handle power nodes only");

  using CoarseFEType = typename decltype(coarseLocalView)::Tree::FiniteElement;
  using FunctionBaseClass = typename Dune::LocalFiniteElementFunctionBase<CoarseFEType>::type;
  using LocalBasisWrapper = LocalBasisComponentWrapper<typename CoarseFEType::Traits::LocalBasisType, FunctionBaseClass>;

  matrix.setSize(rows,cols);
  matrix.setBuildMode(MatrixType::random);


  // ///////////////////////////////////////////
  // Determine the indices present in the matrix
  // ///////////////////////////////////////////

    // Only handle every dof once
  Dune::BitSetVector<1> processed(fineBasis.size());

  Dune::MatrixIndexSet indices(rows, cols);

  // loop over all fine grid elements
  for ( const auto& fineElement : elements(fineGridView) )
  {
    // find a coarse element that is the parent of the fine element
    // start in the fine grid and track the geometry mapping
    auto fE = fineElement;
    // collect all geometryInFather's on the way
    std::vector<decltype(fineElement.geometryInFather())> geometryInFathersVector;
    while ( not coarseGridView.contains(fE) and fE.hasFather() )
    {
      // add the geometry to the container
      geometryInFathersVector.push_back(fE.geometryInFather());
      // step up one level
      fE = fE.father();
    }

    // did we really end up in coarseElement?
    if ( not coarseGridView.contains(fE) )
      DUNE_THROW( Dune::Exception, "There is a fine element without a parent in the coarse grid!");

    const auto& coarseElement = fE;

    const auto geometryInFathers = GeometryInFathers<decltype(geometryInFathersVector)>(geometryInFathersVector);

    coarseLocalView.bind(coarseElement);
    fineLocalView.bind(fineElement);

    // get the root as reference
    const auto& node = fineLocalView.tree();

    // get the local basis
    const auto& localBasis = node.finiteElement().localBasis();
    // Hack: The RangeFieldType is the best guess of a suitable type for coefficients we have here
    using CoefficientType = typename std::decay_t<decltype(localBasis)>::Traits::RangeFieldType;

    std::vector<CoefficientType> coefficients(localBasis.size());

    LocalBasisWrapper basisFctj(node.finiteElement().localBasis(),0);

    const auto& coarseLocalBasis = coarseLocalView.tree().finiteElement().localBasis();

    for (size_t j=0; j<coarseLocalBasis.size(); j++)
    {
      // wrap each local basis function as a local function.
      basisFctj.setIndex(j);

      // transform the local fine function to a local coarse function
      const auto coarseBaseFctj = OtherGeometryWrapper<decltype(basisFctj),decltype(geometryInFathers)>(basisFctj, geometryInFathers);

      // Interpolate j^th base function by the fine basis
      node.finiteElement().localInterpolation().interpolate(coarseBaseFctj, coefficients);

      // get the matrix col index
      const auto& globalCoarseIndex = coarseLocalView.index(j);

      // set the matrix indices
      for (size_t i=0; i<localBasis.size(); i++) {

        // some values may be practically zero -- no need to store those
        if ( std::abs(coefficients[i]) < tolerance )
            continue;

        // get the matrix row index
        const auto& globalFineIndex = fineLocalView.index(i);

        if (processed[globalFineIndex][0])
          continue;

        indices.add(globalFineIndex, globalCoarseIndex);
      }
    }

    // now the all dof's of this fine element to 'processed'
    for (size_t i=0; i<localBasis.size(); i++)
    {
      const auto& globalFineIndex = fineLocalView.index(i);
      processed[globalFineIndex] = true;
    }
  }

  indices.exportIdx(matrix);
  matrix = 0;

  // reset all dof's
  processed.unsetAll();

  //////////////////////
  // Now set the values
  //////////////////////

  for ( const auto& fineElement : elements(fineGridView) )
  {
    // find a coarse element that is the parent of the fine element
    // start in the fine grid and track the geometry mapping
    auto fE = fineElement;
    // collect all geometryInFather's on the way
    std::vector<decltype(fineElement.geometryInFather())> geometryInFathersVector;
    while ( not coarseGridView.contains(fE) and fE.hasFather() )
    {
      // add the geometry to the container
      geometryInFathersVector.push_back(fE.geometryInFather());
      // step up one level
      fE = fE.father();
    }

    // did we really end up in coarseElement?
    if ( not coarseGridView.contains(fE) )
      DUNE_THROW( Dune::Exception, "There is a fine element without a parent in the coarse grid!");

    const auto& coarseElement = fE;

    const auto geometryInFathers = GeometryInFathers<decltype(geometryInFathersVector)>(geometryInFathersVector);

    coarseLocalView.bind(coarseElement);
    fineLocalView.bind(fineElement);

    // get the root as reference
    const auto& node = fineLocalView.tree();

    // get the local basis
    const auto& localBasis = node.finiteElement().localBasis();
    using Coefficient = typename std::decay_t<decltype(localBasis)>::Traits::RangeFieldType;

    std::vector<Coefficient> coefficients(localBasis.size());

    LocalBasisWrapper basisFctj(node.finiteElement().localBasis(),0);

    const auto& coarseLocalBasis = coarseLocalView.tree().finiteElement().localBasis();

    for (size_t j=0; j<coarseLocalBasis.size(); j++)
    {
      // wrap each local basis function as a local function.
      basisFctj.setIndex(j);

      // transform the local fine function to a local coarse function
      const auto coarseBaseFctj = OtherGeometryWrapper<decltype(basisFctj),decltype(geometryInFathers)>(basisFctj, geometryInFathers);

      // Interpolate j^th base function by the fine basis
      node.finiteElement().localInterpolation().interpolate(coarseBaseFctj, coefficients);

      // get the matrix col index
      const auto& globalCoarseIndex = coarseLocalView.index(j);

      // set the matrix indices
      for (size_t i=0; i<localBasis.size(); i++) {

        // some values may be practically zero -- no need to store those
        if ( std::abs(coefficients[i]) < tolerance )
            continue;

        // get the matrix row index
        const auto& globalFineIndex = fineLocalView.index(i);

        if (processed[globalFineIndex][0])
          continue;

        Dune::MatrixVector::addToDiagonal(matrix[globalFineIndex][globalCoarseIndex],coefficients[i]);
      }
    }

     // now the all dof's of this fine element to 'processed'
    for (size_t i=0; i<localBasis.size(); i++)
    {
      const auto& globalFineIndex = fineLocalView.index(i);
      processed[globalFineIndex] = true;
    }
  }
}


#endif
