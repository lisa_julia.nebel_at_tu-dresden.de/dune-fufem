// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef BASIS_TRACE_INDEXSET_HH
#define BASIS_TRACE_INDEXSET_HH

#include <dune/fufem/boundarypatch.hh>

#include <dune/localfunctions/common/localkey.hh>

/** \brief An index set of the trace of a finite element space. */
template <class GlobalBasis>
class BasisTraceIndexSet
{
private:
    typedef typename GlobalBasis::GridView GridView;
    typedef typename GlobalBasis::LocalFiniteElement LocalFiniteElement;
    typedef BoundaryPatch<GridView> BoundaryPatchType;

public:
    BasisTraceIndexSet(const GlobalBasis& globalBasis, const BoundaryPatchType& boundary) :
        basis_(globalBasis),
        boundary_(boundary)
    {
        // create a mapping from the full finite element space indices to the trace space indices
        globalToTraceIndex.resize(basis_.size(),-1);

        typedef typename BoundaryPatchType::iterator BoundaryIterator;

        BoundaryIterator it = boundary_.begin();
        BoundaryIterator end = boundary_.end();

        int traceIndex = 0;

        for (;it!=end;++it) {
            const LocalFiniteElement& lfe = basis_.get(*it->inside());

            // if the basis is piecewise constant all dofs belong to the trace
            if (lfe.localBasis().order()==0) {
                globalToTraceIndex[basis_.index(*it->inside(),0)]=traceIndex++;
                continue;
            }

            for (int j=0;j<lfe.localBasis().size();j++) {

                // check if the subentity the dof is attached is contained in the boundary
                Dune::LocalKey key = lfe.localCoefficients().localKey(j);

                // and if yes, give the dof a trace index
                if (it->containsInsideSubEntity(key.subEntity(),key.codim()))
                    if (globalToTraceIndex[basis_.index(*it->inside(),j)]==-1)
                        globalToTraceIndex[basis_.index(*it->inside(),j)]=traceIndex++;
            }

        }

        // we might need that one
        size_=traceIndex+1;

    }

    //! \brief The total number of trace dofs
    int size() {return size_;}

    //! \brief Check if degree of freedom belongs to the trace space
    bool isTraceDof(int index) {return (globalToTraceIndex[index] > -1);}

    //! \brief Map a global index from the full finite element space to the trace space index set
    int index(int fullIndex) {return globalToTraceIndex[fullIndex];}

private:
    const BoundaryPatchType& boundary_;
    const GlobalBasis& basis_;
    std::vector<int> globalToTraceIndex;
    int size_;

};

#endif

