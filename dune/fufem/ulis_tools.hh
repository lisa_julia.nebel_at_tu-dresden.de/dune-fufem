#ifndef ULIS_TOOLS_HH
#define ULIS_TOOLS_HH

#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixindexset.hh>

#include <vector>

#warning This header is deprecated. Please use Arithmetic::addProduct() in place of multiplyFM() and StaticMatrix::transpose() in place of transpose()

// Multiplies Matrices of Dune::FieldMatrix Type
template<class LeftMatrixType, class RightMatrixType>
inline void multiplyFM(const LeftMatrixType& A, const RightMatrixType& B, Dune::FieldMatrix<typename LeftMatrixType::field_type,LeftMatrixType::rows,RightMatrixType::cols >& C)
{
    assert(LeftMatrixType::cols == RightMatrixType::rows);

    int n = LeftMatrixType::rows,
        m = LeftMatrixType::cols,
        k = RightMatrixType::cols;

    C = 0.0;

    for (int row = 0; row < n; ++row)
    {
        for (int col = 0 ; col < k; ++col)
        {
            for (int i = 0; i < m; ++i)
                C[row][col] += A[row][i]*B[i][col];
        }
    }
}

// Transposes Matrices of Dune::FieldMatrix Type
template<class MatrixType, class TransposedMatrixType>
inline void transposeFM(const MatrixType& M, TransposedMatrixType& Mt)
{
    int nRows = MatrixType::rows,
        nCols = MatrixType::cols;
    
    for (int row = 0; row < nRows; ++row)
    {
        for (int col = 0 ; col < nCols; ++col)
        {
            Mt[col][row] = M[row][col];
        }
    }

}

// Transposes Matrices of Dune::BCRSMatrix<FieldMatrix<K,n,m>,?> Type
template<class MatrixType, class TransposedMatrixType>
void transpose(const MatrixType& A, TransposedMatrixType& At)
{
    const int nRows = A.N();
    const int nCols = A.M();

    const int nBlockRows = MatrixType::block_type::rows,
        nBlockCols = MatrixType::block_type::cols;
    
    typename TransposedMatrixType::block_type transposedBlock;
    
    Dune::MatrixIndexSet idxSetA(nRows, nCols);
    idxSetA.import<MatrixType>(A);
    
    Dune::MatrixIndexSet idxSetAt(nCols, nRows);
    
    typedef typename MatrixType::ConstColIterator ColIterator;
    
    for (int row = 0; row < nRows; ++row)
    {
        ColIterator col = A[row].begin();
        ColIterator end = A[row].end();
        for ( ; col != end; ++col)
        {   
            idxSetAt.add(col.index(),row);
        }
    }

    idxSetAt.exportIdx(At);

    if (nBlockRows+nBlockCols > 2)
    {
        for (int row = 0; row < nRows; ++row)
        {
            ColIterator col = A[row].begin();
            ColIterator end = A[row].end();
            for ( ; col != end; ++col)
            {
                transposeFM<typename MatrixType::block_type, typename TransposedMatrixType::block_type >(A[row][col.index()],transposedBlock);
                At[col.index()][row] = transposedBlock;
            }
        }
    }
    else
    {
        for (int row = 0; row < nRows; ++row)
        {
            ColIterator col = A[row].begin();
            ColIterator end = A[row].end();
            for ( ; col != end; ++col)
            {
                At[col.index()][row][0][0] = A[row][col.index()][0][0];
            }
        }
    }
}

#endif
