#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <limits>

#include <dune/common/exceptions.hh>
#include <dune/common/parallel/mpihelper.hh>

#if HAVE_DUNE_ALUGRID
#include <dune/grid/common/gridfactory.hh>
#include <dune/alugrid/grid.hh>
#endif

#include <dune/fufem/test/common.hh>
#include <dune/fufem/geometry/refinedsimplexgeometry.hh>

class MakeRefinedSimplexGeometryTestSuite
{
    public:
    typedef typename RefinedSimplexGeometry<double,2,3>::GlobalCoordinate WorldCoords;
    typedef typename RefinedSimplexGeometry<double,2,3>::LocalCoordinate LocalCoords;

    MakeRefinedSimplexGeometryTestSuite()
    {}


    bool check()
    {
        return check_makeRefinedSimplexGeometry();
    }

    private:

    bool check_makeRefinedSimplexGeometry()
    {
        bool passed = true;
#if HAVE_DUNE_ALUGRID
        typedef Dune::ALUGrid<2,3,Dune::simplex, Dune::conforming> GridType;

        GridType* grid;

        Dune::GridFactory<GridType> factory;

        std::vector<WorldCoords> vertices(3);
        std::vector<std::vector<unsigned int> > faces(1);

        double sqrt_two = std::sqrt(2.0);
        double sqrt_six = std::sqrt(6.0);

        vertices = {{-sqrt_two/4, -sqrt_six/4, sqrt_two/2}, {sqrt_two/2, 0, sqrt_two/2}, {-sqrt_two/4, sqrt_six/4, sqrt_two/2}};
        faces = {{0,1,2}};

        for (auto vertex : vertices)
        {
            factory.insertVertex(vertex);
        }

        for (auto face : faces)
        {
            factory.insertElement(Dune::GeometryType(Dune::GeometryType::simplex,2),face);
        }

        grid = factory.createGrid();

        auto eltIt = grid->leafGridView().begin<0>();
        MapToSphere<WorldCoords,WorldCoords> parameterization;

        RefinedSimplexGeometry<typename GridType::ctype, 2,3> refinedGeometry = makeRefinedSimplexGeometry(*eltIt,parameterization);

        {
            WorldCoords testPoint{-sqrt_two/4, 0 , sqrt_two/2}, sollResult(testPoint);
            sollResult /= testPoint.two_norm();
            passed = passed and
                     isCloseDune(sollResult,
                                 refinedGeometry.global(LocalCoords{0, 0.5}));
        }
        {
            WorldCoords testPoint{sqrt_two/8, sqrt_six/8 , sqrt_two/2}, sollResult(testPoint);
            sollResult /= testPoint.two_norm();
            passed = passed and
                     isCloseDune(sollResult,
                                 refinedGeometry.global(LocalCoords{0.5, 0.5}));
        }
        {
            WorldCoords testPoint{sqrt_two/8, -sqrt_six/8 , sqrt_two/2}, sollResult(testPoint);
            sollResult /= testPoint.two_norm();
            passed = passed and
                     isCloseDune(sollResult,
                                 refinedGeometry.global(LocalCoords{0.5, 0}));

            WorldCoords testPoint2{0, 0 , sqrt_two/2}, sollResult2(sollResult);
            sollResult2[0] = sollResult2[1] = 0;
            passed = passed and isCloseDune(sollResult2,
                                            refinedGeometry.global(LocalCoords{
                                                1.0 / 3.0, 1.0 / 3.0}));
        }
#endif
        return passed;
    }


    template <class DomainVector, class RangeVector>
    class MapToSphere
    {
        private:
        const double radius_;
        const DomainVector center_;

        public:
        MapToSphere(const double radius=1.0, const DomainVector& center=DomainVector{0,0,0}):
            radius_(radius),
            center_(center)
        {}

        void evaluate( const DomainVector &x, RangeVector &y ) const
        {
            y = x;
            y -= center_;
            y /= x.two_norm()/radius_;
            y += center_;
        }
    };
};

int main (int argc, char *argv[])
{
      Dune::MPIHelper::instance(argc, argv);
      MakeRefinedSimplexGeometryTestSuite testSuite;
#if HAVE_DUNE_ALUGRID
      return testSuite.check() ? 0 : 1;
#else
      return 77;
#endif
}
