#ifndef FUNCTIONSPACE_BASIS_HH
#define FUNCTIONSPACE_BASIS_HH

/**
   @file
   @brief

   @author
 */

#include <dune/geometry/type.hh>
#include <dune/common/bitsetvector.hh>
#include "dune/fufem/functionspacebases/dofconstraints.hh"


template <class GV, class RT, class LFE>
class FunctionSpaceBasis
{
    protected:
        typedef typename GV::Grid::template Codim<0>::Entity Element;

        static const int dim = GV::Grid::dimension;


    public:
        typedef GV GridView;
        typedef RT ReturnType;
        typedef LFE LocalFiniteElement;
        typedef typename Dune::BitSetVector<1> BitVector;
        typedef typename DOFConstraints::LinearCombination LinearCombination;

        FunctionSpaceBasis(const GridView& gridview) :
            gridview_(gridview)
        {}

        virtual size_t size() const
        {
            DUNE_THROW(Dune::NotImplemented, "You called the empty base class method");
        }

        const LocalFiniteElement& getLocalFiniteElement(const Element& e) const
        {
            DUNE_THROW(Dune::NotImplemented, "You called the empty base class method");
        }

        int index(const Element& e, const int i) const
        {
            DUNE_THROW(Dune::NotImplemented, "You called the empty base class method");
        }

        bool isConstrained(const int index) const
        {
            return false;
        }

        const LinearCombination& constraints(const int index) const
        {
            return dummyWeights_;
        }

        const BitVector& isConstrained() const
        {
            if (isConstrained_.size() != (unsigned int)size())
            {
                isConstrained_.resize(size());
                isConstrained_.unsetAll();
            }
            return isConstrained_;
        }

        void update()
        {
            this->update(gridview_);
        }

        virtual void update(const GridView& gridview)
        {
            gridview_ = gridview;
            if (isConstrained_.size() != 0)
                isConstrained();
        }

        const GridView& getGridView() const
        {
            return gridview_;
        }

        virtual ~FunctionSpaceBasis() {}

    protected:
        GridView gridview_;
        const LinearCombination dummyWeights_;
        mutable BitVector isConstrained_;
};

#endif

