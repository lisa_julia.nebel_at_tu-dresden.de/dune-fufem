#ifndef ANY_HH
#define ANY_HH

#include <list>
#include <memory>

#include <dune/common/typetraits.hh>
#include <dune/common/classname.hh>


/** \brief Class to encapsulate objects of any type
 *
 * This class is inspired by boost::any
 */
class any
{
    private:
        class AnyObjectInterface
        {
            public:
                virtual ~AnyObjectInterface() {}

                virtual AnyObjectInterface* clone() const
                {
                    return new AnyObjectInterface(*this);
                }

                virtual std::string className() const
                {
                    return "";
                }
        };

        template <class T>
        class AnyObjectWrapper :
            public AnyObjectInterface
        {
            public:
                AnyObjectWrapper(const T& t) : t_(t) {}

                virtual ~AnyObjectWrapper() {}

                virtual AnyObjectWrapper* clone() const
                {
                    return new AnyObjectWrapper(*this);
                }

                const T& get() const
                {
                    return t_;
                }

                T& get()
                {
                    return t_;
                }

                std::string className() const
                {
                    return Dune::className<T>();
                }

            private:
                T t_;
        };

    public:

        /**
         * \brief Create an empty any object.
         */
        any() :
            p_(0)
        {}

        /**
         * \brief Create an any object storing a copy of some value.
         *
         * \tparam T Type of the stored value
         *
         * \param t Store a copy of this value
         */
        template <class T>
        any(const T& t)
        {
            p_ = new AnyObjectWrapper<T>(t);
        }

        /**
         * \brief Copy content of another any object.
         *
         * \param other Copy content of this any object.
         *
         * The current content of this any object is destroyed.
         */
        any(const any& other)
        {
            if (other.p_)
                p_ = other.p_->clone();
            else
                p_ = 0;
        }

        /**
         * \brief Copy content of another any object.
         *
         * \param other Copy content of this any object.
         *
         * The current content of this any object is destroyed.
         */
        any& operator= (const any & other)
        {
            AnyObjectInterface* pOld = p_;
            if (other.p_)
                p_ = other.p_->clone();
            else
                p_ = 0;
            delete pOld;
            return *this;
        }

        /**
         * \brief Destroy any object and its content.
         */
        ~any()
        {
            delete p_;
        }

        template<class T>
        T* get()
        {
            AnyObjectWrapper<T>* wrappedT = dynamic_cast<AnyObjectWrapper<T>*>(p_);
            if (wrappedT==0)
                return 0;
            return &(wrappedT->get());
        }

        template<class T>
        const T* get() const
        {
            const AnyObjectWrapper<T>* wrappedT = dynamic_cast<const AnyObjectWrapper<T>*>(p_);
            if (wrappedT==0)
                return 0;
            return &(wrappedT->get());
        }

        std::string className() const
        {
            if (p_)
                return p_->className();
            return "";
        }

    private:
        AnyObjectInterface* p_;
};



/**
 * \brief Exception for failed any_cast calls
 */
struct bad_any_cast {};



/**
 * \brief Try to cast reference to an any object to a const value or reference
 *
 * If the any object does not contain an object of this type a bad_any_cast exception is thrown.
 */
template<typename T>
T any_cast(const any& operand)
{
    using RawT = std::decay_t<T>;
    const RawT* t = operand.template get<RawT>();
    if (t==0)
        throw bad_any_cast();
    return *t;
}



/**
 * \brief Try to cast reference to an any object to a value or reference
 *
 * If the any object does not contain an object of this type a bad_any_cast exception is thrown.
 */
template<typename T>
T any_cast(any& operand)
{
    using RawT = std::decay_t<T>;
    RawT* t = operand.template get<RawT>();
    if (t==0)
        throw bad_any_cast();
    return *t;
}



/**
 * \brief Try to cast a pointer to a const any object to a pointer to a const value
 *
 * Returns 0 if the cast fails.
 */
template<typename T>
const T* any_cast(const any* operand)
{
    return operand->template get<typename Dune::remove_const<T>::type>();
}



/**
 * \brief Try to cast a pointer to a any object to a pointer to a value
 *
 * Returns 0 if the cast fails.
 */
template<typename T>
T* any_cast(any* operand)
{
    return operand->template get<typename Dune::remove_const<T>::type>();
}



/**
 * \brief Container for any type of shared_ptr objects
 *
 * This class stores shared_ptr<T> objects for any type T in a list.
 * Its purpose is to control life time of objects allocated on the heap
 * without storing a separate shared_ptr for each object.
 */
class any_shared_ptr_list :
    public std::list<any>
{
    public:

        /**
         * \brief Store a copy of the shared_ptr in the container.
         *
         * \returns Reference to the stored shared_ptr
         */
        template<class T>
        std::shared_ptr<T>& push_back_ptr(std::shared_ptr<T>& t)
        {
            this->push_back(any(t));
            return any_cast<typename std::shared_ptr<T>&>(this->back());
        }

        /**
         * \brief Create a shared_ptr and store it in the container.
         *
         * This creates a shared_ptr encapsulating the raw pointer and stores
         * it in the container. This e.g. allows to do
         * \code
         * any_shared_ptr_list c;
         * A* p1 = c.push_back_ptr(new SomeClassDerivedFromA).get();
         * B* p2 = c.push_back_ptr(new SomeClassDerivedFromB).get();
         * B* p3 = c.push_back_ptr(new AnotherClassDerivedFromB).get();
         * \endcode
         * Here the container takes ownership for the created objects and thus
         * controls their life time. If you want to share ownership you can just
         * copy the returned shared_ptr.
         *
         * \returns Reference to the stored shared_ptr
         */
        template<class T>
        std::shared_ptr<T>& push_back_ptr(T* t)
        {
            std::shared_ptr<T> p(t);
            return push_back_ptr<T>(p);
        }
};

#endif
