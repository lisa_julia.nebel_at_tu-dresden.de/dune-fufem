// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef VIRTUALFUNCTION_TO_BOUNDARYSEGMENT_ADAPTOR_HH
#define VIRTUALFUNCTION_TO_BOUNDARYSEGMENT_ADAPTOR_HH

#include <memory>

#include <dune/common/fvector.hh>
#include <dune/common/function.hh>
#include <dune/grid/common/boundarysegment.hh>

/**
 * \brief Adapter from VirtualFunction to BoundarySegment
 *
 * \tparam dim Dimension of the grid
 * \tparam dimworld World dimension of the grid
 */
template<int dim, int dimworld>
class VirtualFunctionToBoundarySegmentAdapter :
    public Dune::BoundarySegment<dim, dimworld>
{
    public:
        typedef Dune::FieldVector<double,dim-1> DomainType;
        typedef Dune::FieldVector<double,dimworld> RangeType;
        typedef Dune::VirtualFunction<DomainType, RangeType> FunctionType;
        typedef std::shared_ptr<const FunctionType> FunctionSharedPtr;

        /**
         * \brief Construct an adaptor from VirtualFunction
         *
         * Since BoundarySegments are in general handed over as shared_ptr
         * it needs to control the lifetime of the wrapped function.
         * Thus we pass it as shared_ptr.
         *
         * \param f A VirtualFunction implementation of the boundary parametrization
         */
        VirtualFunctionToBoundarySegmentAdapter(const FunctionSharedPtr& f):
            f_(f)
        {}

        virtual RangeType operator() (const DomainType &local) const
        {
            RangeType global;
            f_->evaluate(local, global);
            return global;
        }

    protected:
        const FunctionSharedPtr f_;
};



#endif
