#ifndef LOCAL_BASIS_JACOBIAN_FUNCTION_HH
#define LOCAL_BASIS_JACOBIAN_FUNCTION_HH

/**
   @file localbasisjacobianfunction.hh
   @brief Wrap Jacobian of local basis functions

   @author graeser@math.fu-berlin.de
 */

#include <vector>

#include <dune/common/function.hh>

#include <dune/fufem/functions/cachedcomponentwrapper.hh>


/** \brief Wrap partial derivative of local basis functions
 *
 * \tparam FE The local finite element type
 */
template <class FE, class Base
    =typename Dune::Function<
        const typename FE::Traits::LocalBasisType::Traits::DomainType&,
        typename FE::Traits::LocalBasisType::Traits::JacobianType&> >
class LocalBasisJacobianFunction :
    public CachedComponentWrapper<
        LocalBasisJacobianFunction<FE, Base>,
        typename std::vector<typename FE::Traits::LocalBasisType::Traits::JacobianType>,
        Base>
{
    public:
        typedef typename std::vector<typename FE::Traits::LocalBasisType::Traits::JacobianType> AllRangeType;
        typedef typename FE::Traits::LocalBasisType LocalBasis;
        typedef typename FE::Traits::LocalBasisType::Traits::DomainType DomainType;
        typedef typename FE::Traits::LocalBasisType::Traits::JacobianType RangeType;

    private:
        typedef LocalBasisJacobianFunction<FE, Base> MyType;
        typedef CachedComponentWrapper<MyType, AllRangeType, Base> BaseClass;

    public:

        /** \brief Construct function from local basis.
         *
         * \param localBasis Local basis
         * \param i Basis function to evaluate next
         */
        LocalBasisJacobianFunction(const LocalBasis& localBasis, const int i=0) :
            BaseClass(i),
            localBasis_(localBasis)
        {}

        void evaluateAll(const DomainType& x, std::vector<RangeType>& yy) const
        {
            localBasis_.evaluateJacobian(x, yy);
        }

    private:
        const LocalBasis& localBasis_;
};

#endif

