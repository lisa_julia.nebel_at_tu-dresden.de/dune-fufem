#ifndef VTK_BASIS_WRITER_HH
#define VTK_BASIS_WRITER_HH

#include <memory>

#include <dune/istl/bvector.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>

#include <dune/fufem/functiontools/basisinterpolator.hh>
#include <dune/fufem/functionspacebases/p1nodalbasis.hh>
#include <dune/fufem/functionspacebases/p0basis.hh>

#include <dune/fufem/any.hh>


template<class B>
class VTKBasisWriter :
    public Dune::VTKWriter<typename B::GridView>
{
    public:
        typedef B Basis;
        typedef typename B::ReturnType ReturnType;
        typedef typename B::GridView GridView;

    protected:

        typedef P1NodalBasis<GridView, ReturnType> WriterP1Basis;
        typedef P0Basis<GridView, ReturnType> WriterP0Basis;

        typedef typename Dune::VTKWriter<typename B::GridView> Base;

        typedef typename Dune::BlockVector<typename Dune::FieldVector<double,1> > BVector;
        typedef typename std::shared_ptr<BVector> BVectorPtr;

    public:

        VTKBasisWriter(const Basis& basis) :
            Base(basis.getGridView()),
            basis_(basis),
            gridview_(basis.getGridView())
        {}


        virtual ~VTKBasisWriter()
        {
        }


        using Base::addVertexData;
        using Base::addCellData;
        using Base::write;


        // \todo this assumes that Vector is of block_level 2 meaning it's a BlockVector<FieldVector<K, size> >. is that okay?
        template <class Vector, class FunctionBasis>
        void addP1Interpolation(const Vector& v, const FunctionBasis& b)
        {
            WriterP1Basis p1Basis(gridview_);
            Vector* p1Vector = createdObjects_.push_back_ptr(new Vector).get();
            Functions::interpolate(p1Basis, *p1Vector, Functions::makeFunction(b, v));

            std::stringstream s;
            s << "x" << createdObjects_.size();
            Base::addVertexData(*p1Vector, s.str());
        }


        template <class Vector>
        void addP1Interpolation(const Vector& v)
        {
            addP1Interpolation(v, basis_);
        }


        // \todo this assumes that Vector is of block_level 2 meaning it's a BlockVector<FieldVector<K, size> >. is that okay?
        template <class Vector, class FunctionBasis>
        void addP0Interpolation(const Vector& v, const FunctionBasis& b)
        {
            WriterP0Basis p0Basis(gridview_);
            Vector* p0Vector = createdObjects_.push_back_ptr(new Vector).get();
            Functions::interpolate(p0Basis, *p0Vector, Functions::makeFunction(b, v));

            std::stringstream s;
            s << "x" << createdObjects_.size();
            Base::addCellData(*p0Vector, s.str());
        }


        template <class Vector>
        void addP0Interpolation(const Vector& v)
        {
            addP0Interpolation(v, basis_);
        }


    private:

        const Basis& basis_;
        const GridView gridview_;
        any_shared_ptr_list createdObjects_;
};

#endif

